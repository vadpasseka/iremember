package com.gemicle.core.beans;

public enum ResultCode {
	
	SUCCESS(0, "Operation was successful"),
    PERMISSION_DENIED(1, "Permission denied"),
    CONNECTION_REFUSED(2, "Connection refused"),
    BAD_CREDENTIALS(3, "Wrong login or password"),
    USER_ALREADY_SAVED(4, "User already saved"),
    FAIL(5, "Fail"),
    USER_SAVED(6, "User saved"),
    WRONG_DATA(7, "Wrong data");

    private Integer code;
	private String message;
	
	
	private ResultCode(Integer code, String message){
		this.code = code;
		this.message = message;
	}
	
	public Integer getCode() {
		return code;
	}

    public String getMessage() {
        return message;
    }

    public static ResultCode getCodeByValue(String code){
        ResultCode[] resultCode = ResultCode.values();
        for (ResultCode currentCode : resultCode){
            if(currentCode.getMessage().equals(code)){
                return currentCode;
            }
        }
        return null;
    }

}
