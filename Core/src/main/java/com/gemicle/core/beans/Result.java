package com.gemicle.core.beans;

public class Result<T> {
	
	private ResultCode code;
	private T resultObject;
	
	public Result(ResultCode code) {
		this.code = code;
	}
	
	public Result(T resultObject) {
		code = ResultCode.SUCCESS;
		this.resultObject = resultObject;
	}
	
	
	public ResultCode getCode() {
		return code;
	}
	
	public T getResultObject() {
		return resultObject;
	}
	
	public void setResultObject(T resultObject) {
		this.resultObject = resultObject;
	}

}
