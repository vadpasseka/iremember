package com.gemicle.core.beans.requests;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gemicle.core.beans.Constants;

/**
 * send query with application/json content type
 */
public class JsonQuery implements Query {

    private static ObjectMapper mapper = new ObjectMapper();

	@Override
	public HttpResponse sendQuery(Object data, String url, Map<String, String> headers) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		try {
            if(headers != null && headers.containsKey(Constants.COOKIES)){
                httppost.setHeader(Constants.COOKIES, headers.remove(Constants.COOKIES));
            }
            timeout(httppost, 2);
			httppost.setHeader(Constants.CONTENT_TYPE,
					Constants.APPLICATION_JSON);
            if(headers != null && headers.containsKey(Constants.COOKIES)){
                httppost.setHeader(Constants.COOKIES, headers.get(Constants.COOKIES));
            }
			httppost.setHeader("User-Agent",
					"Android app");
			httppost.setHeader("Accept",
					"*/*");
			httppost.setHeader("X-Requested-With",
					"XMLHttpRequest");
            String json = mapper.writeValueAsString(data);//convert(data);
            Log.i("json", json);
			httppost.setEntity(new StringEntity(json, HTTP.UTF_8));
			HttpResponse response = httpclient.execute(httppost);
			return response;
		} catch (ClientProtocolException e) {
			Log.e(Constants.QUERY_TAG, e.getMessage());
		} catch (IOException e) {
			Log.e(Constants.QUERY_TAG, e.getMessage());}
//		} catch (JSONException e) {
//            e.printStackTrace();
//        }
        return null;
	}



	/**
	 * 
	 * convert map to JSONObject
	 *
	 * @return
	 */
    public static String convert(Object object) throws JSONException, JsonProcessingException {
        if (object instanceof Map) {
            JSONObject json = new JSONObject();
            Map map = (Map) object;
            for (Object key : map.keySet()) {
                json.put(key.toString(), convert(map.get(key)));
            }
            return json.toString();
        } else if (object instanceof Iterable) {
            JSONArray json = new JSONArray();
            for (Object value : ((Iterable)object)) {
                json.put(value);
            }
            return json.toString();
        } else {
            return mapper.writeValueAsString(object);
        }
    }

    private void timeout(final HttpPost method, int seconds){
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (method != null) {
                    method.abort();
                }
            }
        };
        new Timer(true).schedule(task, seconds * 1000);
    }

}
