package com.gemicle.core.beans.requests;

import java.util.Map;

import org.apache.http.HttpResponse;

public interface Query {

	HttpResponse sendQuery(Object data, String url, Map<String , String> cookies);
	
}
