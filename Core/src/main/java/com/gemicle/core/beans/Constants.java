package com.gemicle.core.beans;

public class Constants {

//    public static final String URL = "http://192.168.0.155:8085";
    public static final String URL = "http://yemi.info/DCWeb";

	//headers - keys
	public static final String CONTENT_TYPE = "Content-Type";
	
	//headers - values
	public static final String APPLICATION_JSON = "application/json;charset=UTF-8";
	
	
	public static final String UTF_8 = "UTF-8";
	
	//tags
	public static final String QUERY_TAG = "query";
	public static final String INFO_TAG = "info";
	public static final String ERROR_TAG = "error";
	
	
	//vacancies
	public static final String COUNT = "count";
	public static final String VACANCIES = "vacancies";
	
	//Intent extra keys
	public static final String INTENT_VACANCY = "vacancy";
    public static final String INTENT_POLL_ID = "pollId";

	//request params
	public static final String PAGE = "page";
	public static final String CURRENCY = "currency";
	public static final String CATEGORY = "categories";
	public static final String SEARCH_PERIOD = "searchPeriod";
	public static final String LOCATIONS = "locations";
	public static final String FROM_SALARY = "fromSalary";
	public static final String TO_SALARY = "toSalary";
	public static final String KEYWORD = "keyword";

	public static final Integer NUMBER_VACANCIES_ON_PAGE = 20;

	//Vacancy intent keys
	public static final String VACANCY_TITLE = "vacancy.title";
	public static final String VACANCY_SALARY = "vacancy.salary";
	public static final String VACANCY_DESCRIPTION = "vacancy.description";
	public static final String VACANCY_CONTACTS = "vacancy.contacts";

	//put data keys
	public static final String VACANCY_PARCELAR_KEY = "vacancy";
    public static final String VACANCY_SOURCE_PAGE = "source_page";
    public static final String VACANCY_SELECTED_ITEM = "selected_item";

    //registration request keys
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String ROLE = "role";
    public static final String COOKIES = "cookie";

    //add product
    public static final String FILE = "file";
}
