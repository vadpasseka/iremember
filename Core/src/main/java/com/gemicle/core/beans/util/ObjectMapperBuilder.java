package com.gemicle.core.beans.util;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Builder for ObjectMapper
 */
public class ObjectMapperBuilder {

    private static ObjectMapperBuilder mapperBuilder;

    private static final DateFormat defaultDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");


    private ObjectMapper mapper;

    public ObjectMapperBuilder(){
        mapper = new ObjectMapper();
        mapper.setDateFormat(defaultDateFormat);
        new JsonToStringModule(mapper);
    }

    public static ObjectMapperBuilder getInstance(){
        if(mapperBuilder == null){
            mapperBuilder = new ObjectMapperBuilder();
        }
        return mapperBuilder;
    }

    public ObjectMapper getMapper() {
        return mapper;
    }
}
