package com.gemicle.core.beans;

import android.content.Context;
import android.content.SharedPreferences;

import com.gemicle.core.beans.Constants;

public class AuthPreferences {

    private static final String KEY_USER = "user";
    private static final String KEY_TOKEN = "token";

    private SharedPreferences preferences;

    public AuthPreferences(Context context) {
        preferences = context
                .getSharedPreferences("auth", Context.MODE_PRIVATE);
    }

    public void setUser(String user) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_USER, user);
        editor.commit();
    }


    public void setCookies(String cookies) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.COOKIES, cookies);
        editor.commit();
    }

    public String getCookies() {
        return preferences.getString(Constants.COOKIES, null);
    }


    public void setToken(String password) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_TOKEN, password);
        editor.commit();
    }

    public String getUser() {
        return preferences.getString(KEY_USER, null);
    }

    public String getToken() {
        return preferences.getString(KEY_TOKEN, null);
    }
}