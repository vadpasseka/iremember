package com.gemicle.core.beans.util;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

public class JsonToStringModule {

	public JsonToStringModule(ObjectMapper mapper) {
		SimpleModule testModule = new SimpleModule("ObjectIdModule",
				new Version(1, 0, 0, null));
		testModule.addSerializer(ObjectId.class, new ToStringSerializer());
		mapper.registerModule(testModule);
	}

}
