package com.gemicle.core.beans.requests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.gemicle.core.beans.Constants;

import android.util.Log;

public class PostQuery implements Query {

	@Override
	public HttpResponse sendQuery(Object data, String url, Map<String, String> headers) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		try {
            if(headers != null && headers.containsKey(Constants.COOKIES)){
                httppost.setHeader(Constants.COOKIES, headers.get(Constants.COOKIES));
            }
            Map<String, Object> request = (Map<String, Object>)data;
            Set<String> keys = request.keySet();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			for (String key : keys) {
				nameValuePairs.add(new BasicNameValuePair(key, (String) request.get(key)));
			}
			httppost.setHeader("Content-Type",
					"application/x-www-form-urlencoded;charset=UTF-8");
			httppost.setHeader("User-Agent",
					"Android app");
			httppost.setHeader("Accept",
					"*/*");
			httppost.setHeader("X-Requested-With",
					"XMLHttpRequest");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);
			return response;
		} catch (ClientProtocolException e) {
			Log.e(Constants.QUERY_TAG, e.getMessage());
		} catch (IOException e) {
			Log.e(Constants.QUERY_TAG, e.getMessage());
		}
		return null;
	}

}
