package com.gemicle.core.beans;

public interface ITaskNotification<T> {

    void completed(T data);

}
