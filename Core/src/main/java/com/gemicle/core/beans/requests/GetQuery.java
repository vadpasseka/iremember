package com.gemicle.core.beans.requests;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

import com.gemicle.core.beans.Constants;

public class GetQuery implements Query {

	@Override
	public HttpResponse sendQuery(Object data, String url, Map<String, String> headers) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(convert(url, (Map<String, Object>)data));
		try {
            if(headers != null && headers.containsKey(Constants.COOKIES)){
                httpget.setHeader(Constants.COOKIES, headers.get(Constants.COOKIES));
            }
            timeout(httpget, 5);
			HttpResponse response = httpclient.execute(httpget);
			return response;
		} catch (ClientProtocolException e) {
			Log.e(Constants.QUERY_TAG, e.getMessage());
		} catch (IOException e) {
			Log.e(Constants.QUERY_TAG, e.getMessage());
		}
		return null;
	}

	
	private String convert(String url, Map<String, Object> data){
		if(data == null || data.isEmpty()){
			return url;
		}
		url += "?";
		Set<String> keys = data.keySet();
		for (String key : keys) {
			url += key + "=" + data.get(key);
		}
		return url;
	}

    private void timeout(final HttpGet getMethod, int seconds){
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (getMethod != null) {
                    getMethod.abort();
                }
            }
        };
        new Timer(true).schedule(task, seconds * 1000);
    }
}
