package com.gemicle.core.beans.service;

/**
 * replace special sign
 * <p/>
 * <h1>examples:</h1>
 * <ul>
 * <li>&quot; to "</li>
 * <li>&laquo; to "</li>
 * <li>&raquo; to "</li>
 * <li>&copy; to "</li>
 * </ul>
 */
public class FilterTextService {

    private static final String QUOT = "&quot;";
    private static final String LAQUO = "&laquo;";
    private static final String RAQUO = "&raquo;";
    private static final String COPY = "&copy;";

    private static final String QUOT_RESULT = "\"";
    private static final String LAQUO_RESULT = "«";
    private static final String RAQUO_RESULT = "»";
    private static final String COPY_RESULT = "©";

    public static String filterSpecialSign(String data){
        if(data == null){
            return "";
        }
        String filteredData = filter(data, QUOT, QUOT_RESULT);
        filteredData = filter(filteredData, LAQUO , LAQUO_RESULT);
        filteredData = filter(filteredData, COPY , COPY_RESULT);
        return filter(filteredData, RAQUO , RAQUO_RESULT);
    }

    private static String filter(String data , String regexp , String replacement){
        return data.replaceAll(regexp, replacement);
    }

}
