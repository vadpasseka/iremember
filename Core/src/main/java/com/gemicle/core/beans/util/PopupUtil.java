package com.gemicle.core.beans.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.gc.materialdesign.views.ProgressBarDeterminate;
import com.gc.materialdesign.views.ProgressBarIndeterminate;
import com.gc.materialdesign.views.ProgressBarIndeterminateDeterminate;
import com.gc.materialdesign.widgets.ProgressDialog;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Class for work with material popups
 */
public class PopupUtil {

    /**
     *
     * Show material dialog with ok and cancel buttons
     *
     * @param context application context
     * @param ok text ok button
     * @param cancel text cancel button
     * @param message message inside dialog
     * @param action dialog actions callback
     */
    public static void showDialog(Context context, int ok, int cancel, int message, final IDialogAction action){
        Resources resources = context.getResources();
        showDialog(context, ok , cancel, resources.getString(message), action);
    }

    public static void showDialog(Context context, int ok, int message, final IDialogAction action){
        Resources resources = context.getResources();
        final MaterialDialog mMaterialDialog = new MaterialDialog(context);
        mMaterialDialog.setMessage(resources.getString(message))
                .setPositiveButton(resources.getString(ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        action.accept(mMaterialDialog);
                        mMaterialDialog.dismiss();
                    }
                });
        mMaterialDialog.show();
    }

    public static void showDialog(Context context, int ok, int cancel, String message, final IDialogAction action){
        Resources resources = context.getResources();
        final MaterialDialog mMaterialDialog = new MaterialDialog(context);
        mMaterialDialog.setMessage(message)
                .setPositiveButton(resources.getString(ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        action.accept(mMaterialDialog);
                        mMaterialDialog.dismiss();
                    }
                })
                .setNegativeButton(resources.getString(cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        action.dismiss(mMaterialDialog);
                        mMaterialDialog.dismiss();
                    }
                });
        mMaterialDialog.show();
    }

    public static void showCustomDialog(Context context, int ok, int cancel, View view, final IDialogAction action){
        Resources resources = context.getResources();
        final MaterialDialog mMaterialDialog = new MaterialDialog(context);
        mMaterialDialog.setContentView(view)
                .setPositiveButton(resources.getString(ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        action.accept(mMaterialDialog);

                    }
                })
                .setNegativeButton(resources.getString(cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        action.dismiss(mMaterialDialog);
                        mMaterialDialog.dismiss();
                    }
                });
        mMaterialDialog.show();
    }


    /**
     *
     * @param context application context
     * @param progressDeterminate element in layout
     * @return dialog
     */
    public static MaterialDialog showProgressDialog(Activity context, ProgressBarDeterminate progressDeterminate){
        final MaterialDialog mMaterialDialog = new MaterialDialog(context);
        mMaterialDialog.setView(progressDeterminate);
        return mMaterialDialog;
    }

    public static ProgressBarIndeterminateDeterminate createProgressBar(Activity context, ViewGroup root, int progressBarLayout, int progressDeterminate){
        ProgressBarIndeterminateDeterminate progressBar = (ProgressBarIndeterminateDeterminate)context.getLayoutInflater().inflate(progressBarLayout, root, false)
                .findViewById(progressDeterminate);
        return progressBar;
    }

    public interface IDialogAction{
        void accept(MaterialDialog mMaterialDialog);
        void dismiss(MaterialDialog mMaterialDialog);
    }

    public interface IDefaultDialogAction{
        void accept(DialogInterface mMaterialDialog);
        void dismiss(DialogInterface mMaterialDialog);
    }
}
