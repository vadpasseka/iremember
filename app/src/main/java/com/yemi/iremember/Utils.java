package com.yemi.iremember;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Utils {

    public static String getTimestamp(String date, String time){
        String dt = date + " " + time;
        try {
            DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            Date d = format.parse(dt);
            return String.valueOf(d.getTime());
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static  long getCurrentTimestamp(){
        return Calendar.getInstance().getTimeInMillis();
    }

    public static String validateNumber(int i){
        if(i < 10){
            return "0" + i;
        }
        return String.valueOf(i);
    }

    public static int getRandId(){
        String s = String.valueOf(Calendar.getInstance().getTimeInMillis());
        return Integer.valueOf(s.substring(7, s.length()));

    }

    public static void savePreference(Context context, String key, String value ) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(key, value);
        ed.apply();
    }

    public static String getPreference(Context context, String key) {
        SharedPreferences sPref  = PreferenceManager.getDefaultSharedPreferences(context);
        return sPref.getString(key, "");
    }
}
