package com.yemi.iremember.receivers;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.yemi.iremember.Constants;
import com.yemi.iremember.been.MessageStatus;
import com.yemi.iremember.dao.TaskDao;
import com.yemi.iremember.services.AlarmService;

import java.util.Calendar;


public class SmsStatusBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int smsId = intent.getIntExtra(Constants.SMS_ID, 0);
        TaskDao.init(context);
        if(getResultCode() == Activity.RESULT_OK){
            TaskDao.updateTaskById(smsId, MessageStatus.SENT.toString());
        } else {
            TaskDao.updateTaskById(smsId, MessageStatus.FAIL.toString());
            Toast.makeText(context, "Sms sending failed", Toast.LENGTH_SHORT).show();
        }
        Intent listUpdater = new Intent();
        listUpdater.setAction(Constants.INTENT_FILTER_ACTION);
        context.sendBroadcast(listUpdater);
        AlarmService.removeIntent(context, smsId);
        TaskDao.deactivate();
        Log.i("SMS_RECEIVER", String.valueOf(smsId));
    }
}
