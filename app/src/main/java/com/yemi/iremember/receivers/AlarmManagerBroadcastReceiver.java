package com.yemi.iremember.receivers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import com.yemi.iremember.Constants;
import com.yemi.iremember.SmsUtils;
import com.yemi.iremember.been.MessageStatus;
import com.yemi.iremember.been.Task;
import com.yemi.iremember.dao.TaskDao;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Calendar;
import java.util.Map;


public class AlarmManagerBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Long smsId = intent.getLongExtra(Constants.SMS_ID, 0);
        getSmsData(context, smsId);

        Log.i("service started in : ", Calendar.getInstance().getTime().toString());
    }


    public void getSmsData(Context context, Long taskId) {
        ObjectMapper mapper = new ObjectMapper();
        TaskDao.init(context);
        Cursor c = TaskDao.getCursor();
        int rowId = c.getColumnIndex(TaskDao.TASK_ID);
        int taskColumnId = c.getColumnIndex(TaskDao.TASK);
        if (c.moveToFirst()) {
            do {
                try {
                    Task task = mapper.readValue(c.getString(taskColumnId), Task.class);
                    if(c.getInt(rowId) == taskId && task.getStatus().equals("in query")){
                        task.setId(c.getInt(rowId));
                        send(task,context);
                        TaskDao.deactivate();
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } while (c.moveToNext());
        }
    }


    public void send(Task task, Context context) {
        Map<String, String> contacts = task.getContacts();
        for (Map.Entry<String, String> entry : contacts.entrySet()) {
            SmsUtils.send(task.getMessage(), entry.getKey(), context, task.getId());

            Log.i("sms sent - ", task.toString());
        }
    }
}
