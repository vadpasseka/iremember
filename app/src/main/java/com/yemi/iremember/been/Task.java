package com.yemi.iremember.been;


import com.yemi.iremember.Utils;

import java.util.Comparator;
import java.util.Map;

public class Task implements Comparator<Task>{
    private Integer id;
    private String timestamp;
    private String time;
    private String date;
    private String message;
    private int intentId;
    private String status = MessageStatus.IN_PROGRESS.getStatus();
    private Map<String, String> contacts;

    public Task() {

    }

    @Override
    public String toString() {
        return id + "\n" +
                timestamp + "\n" +
                time + "\n" +
                date + "\n" +
                message + "\n" +
                status + "\n" +
                contacts.toString();
    }

    public Task(String timestamp, String time,
                String date, String message,
                Map<String, String> contacts) {
        this.timestamp = timestamp;
        this.time = time;
        this.date = date;
        this.message = message;
        this.contacts = contacts;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, String> getContacts() {
        return contacts;
    }

    public void setContacts(Map<String, String> contacts) {
        this.contacts = contacts;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIntentId() {
        return intentId;
    }

    public void setIntentId(int intentId) {
        this.intentId = intentId;
    }

    @Override
    public int compare(Task task, Task task2) {
        return Long.valueOf(Utils.getTimestamp(task2.getDate(), task2.getTime())).compareTo(Long.valueOf(Utils.getTimestamp(task.getDate(), task.getTime())));
    }
}
