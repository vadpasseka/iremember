package com.yemi.iremember.been;

public class Contact {
    String name;
    String phone;
    String latter;
    Boolean checked;

    public Contact(String name, String phone, String latter, Boolean checked){
        this.name = name;
        this.latter = latter;
        this.phone = phone.replace(" ", "");
        this.checked = checked;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLatter() {
        return latter;
    }

    public void setLatter(String latter) {
        this.latter = latter;
    }

    @Override
    public String toString() {
        return name + "\n" + phone;
    }

}
