package com.yemi.iremember.been;

public enum MessageStatus {

    SENT("sent") , IN_PROGRESS("in query"), FAIL("fail");

    String status;

    MessageStatus(String status){
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return status;
    }
}
