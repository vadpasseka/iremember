package com.yemi.iremember.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.TextView;

import com.yemi.iremember.Constants;
import com.yemi.iremember.Utils;
import com.yemi.iremember.been.Task;
import com.example.serhiy.calenarnot.R;
import com.yemi.iremember.dao.TaskDao;
import com.yemi.iremember.preference.RememberPreferences;
import com.google.analytics.tracking.android.EasyTracker;;

import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @InjectView(R.id.tvIRemember)
    TextView tvIRemember;

    @InjectView(R.id.tvSMS)
    TextView tvSMS;

    @InjectView(R.id.tvSheduleSms)
    TextView tvSheduleSms;

    @InjectView(R.id.chbDontShow)
    CheckBox checkBox;

    private RememberPreferences rememberPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rememberPreferences = new RememberPreferences(this);
        if(rememberPreferences.isRemember()){
            startMainActivity();
            return;
        }
        setContentView(R.layout.loader_layout);
        ButterKnife.inject(this);
        Typeface boldTf = Typeface.createFromAsset(getAssets(), "MyriadProBold.ttf");
        Typeface regularTf = Typeface.createFromAsset(getAssets(), "MyriadProRegular.ttf");
        tvIRemember.setTypeface(boldTf);
        tvSMS.setTypeface(boldTf);
        tvSheduleSms.setTypeface(regularTf);
        checkBox.setTypeface(regularTf);
    }

    @OnClick(R.id.btnSheduleSms)
    public void btnSheduleSmsClick(){
        if(checkBox.isChecked()){
            rememberPreferences.saveRemember(true);
        }
        startMainActivity();
    }

    private void startMainActivity(){
        TaskDao.init(getApplicationContext());
        List<Task> tasks = TaskDao.getTasks();
        Intent intent = null;
        if(tasks.isEmpty()){
            intent = new Intent(MainActivity.this, AddSmsActivity.class);
        }
        else{
            intent = new Intent(MainActivity.this, SmsListActivity.class);
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this); // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this); // Add this method.
    }

}
