package com.yemi.iremember.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yemi.iremember.Constants;
import com.yemi.iremember.Utils;
import com.yemi.iremember.been.Task;
import com.example.serhiy.calenarnot.R;
import com.yemi.iremember.dao.TaskDao;
import com.yemi.iremember.listeners.ValidationCallback;
import com.yemi.iremember.listeners.ValidatorListener;
import com.yemi.iremember.receivers.AlarmManagerBroadcastReceiver;
import com.yemi.iremember.services.AlarmService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gc.materialdesign.views.ButtonRectangle;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class AddSmsActivity extends Activity {
    private Map<String, String> contacts = new HashMap<String, String>();

    @NotEmpty(messageResId = R.string.calendar_empty)
    @InjectView(R.id.tvCalendar)
    TextView tvCalendar;

    @NotEmpty(messageResId = R.string.date_empty)
    @InjectView(R.id.tvClock)
    TextView tvClock;

    @NotEmpty(messageResId = R.string.recipients_empty)
    @InjectView(R.id.tvNetwork)
    TextView recipients;

    @InjectView(R.id.tvToolbarAddDSheduledSms)
    TextView tvToolbarAdDSheduledSms;

    @NotEmpty(messageResId = R.string.message_empty)
    @InjectView(R.id.etText)
    EditText messageBody;

    @InjectView(R.id.btnAddSms)
    ButtonRectangle btnAdd;

    private ObjectMapper mapper = new ObjectMapper();

    private Integer id;
    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_sheduled_sms_layout);
        ButterKnife.inject(this);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        validator = new Validator(this);
        validator.setValidationListener(new ValidatorListener(this, new ValidationCallback() {
            @Override
            public void success() {
                String date = tvCalendar.getText().toString();
                String time = tvClock.getText().toString();
                String timestamp = Utils.getTimestamp(date, time);
                int intentId = Utils.getRandId();
                Task task = new Task(timestamp, time, date,
                        messageBody.getText().toString(), contacts);
                task.setIntentId(intentId);
                TaskDao.init(getApplicationContext());
                if(id != null){
                    task.setId(id);
                    TaskDao.update(task, id);
                }
                else{
                    id = (int)TaskDao.create(task);
                }
                setBroadcastReceiver(getCurrentFocus(), id, intentId);
                TaskDao.deactivate();
                Intent i = new Intent(AddSmsActivity.this, SmsListActivity.class);
                startActivity(i);
                finish();
            }

            @Override
            public void fail() {

            }
        }));



        Typeface regularTf = Typeface.createFromAsset(getAssets(), "MyriadProRegular.ttf");
        recipients.setTypeface(regularTf);
        tvCalendar.setTypeface(regularTf);
        tvClock.setTypeface(regularTf);
        messageBody.setTypeface(regularTf);
        tvToolbarAdDSheduledSms.setTypeface(regularTf);


        try {
            String json = getIntent().getStringExtra("data");
            if(json == null){
                return;
            }
            btnAdd.setText(getResources().getString(R.string.txtBtnUpdateSms));
            Task task = mapper.readValue(json, Task.class);
            id = task.getId();
            initData(task);
        } catch (IOException e) {
            e.printStackTrace();
        }





    }


    private void initData(Task task){
        messageBody.setText(task.getMessage());
        showContacts(task.getContacts());
        contacts = task.getContacts();
        tvCalendar.setText(task.getDate());
        tvClock.setText(task.getTime());
    }


    @OnClick(R.id.llSetDay)
    public void llSetDayClick(){
        setDate();
    }

    @OnClick(R.id.ibCalendar)
    public void ibCalendarClick(){
        setDate();
    }

    @OnClick(R.id.llSetTime)
    public void llSetTimeClick(){
        setTime();
    }

    @OnClick(R.id.ibClock)
    public void ibClockClick(){
        setTime();
    }

    @OnClick(R.id.btnDelete)
    public void btnDeleteClick(){
//        recipients.setText("");
//        messageBody.setText("");
//        tvClock.setText("");
//        tvCalendar.setText("");
//        if(!contacts.isEmpty()){
//            contacts.clear();
//        }
        finish();
    }

    @OnClick(R.id.llAddRecipient)
    public void llAddRecipientClick(){
        pickContacts();
    }

    @OnClick(R.id.ibNetwork)
    public void ibNetworkClick(){
        pickContacts();
    }

    @OnClick(R.id.ibSmsList)
    public void ibSmsListClick(){
        Intent i  = new Intent(this, SmsListActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnAddSms)
    public void btnAddSmsClick(){
        validator.validate();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.CONTACT_PICKER_RESULT_CODE) {
            displayCheckedContacts(data);
        }
    }


    private void pickContacts(){
        Intent intent = new Intent(AddSmsActivity.this, ContactPickerActivity.class);
        intent.putExtra("contacts", (HashMap<String, String>) contacts);
        startActivityForResult(intent, Constants.CONTACT_PICKER_RESULT_CODE);
    }


    private void displayCheckedContacts(Intent intent) {
        contacts = (HashMap<String, String>) intent.getSerializableExtra("contacts");
        showContacts(contacts);
    }

    private void showContacts(Map<String, String> data){
        String names = "";
        for (Map.Entry<String, String> entry : data.entrySet()) {
            names += entry.getValue() + ", ";
        }
        if(!names.isEmpty()){
            recipients.setText(names);
        }
    }

    public void setBroadcastReceiver(View view, long id, int intentId) {
        String date = tvCalendar.getText().toString();
        String time = tvClock.getText().toString();
        AlarmService.setAlarm(this, date, time, id, intentId);
    }

    /*CALENDAR*/
    private void setDate(){
        final Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener(){
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int i, int i2, int i3) {
                        String date = i + "/" + Utils.validateNumber((i2 + 1)) + "/" + Utils.validateNumber(i3);
                        tvCalendar.setText(date);
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    private void setTime(){
        final Calendar now1 = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(RadialPickerLayout radialPickerLayout, int i, int i2) {
                        String time = Utils.validateNumber(i) + ":" + Utils.validateNumber(i2);
                        tvClock.setText(time);
                    }
                },
                now1.get(Calendar.HOUR),
                now1.get(Calendar.MINUTE),
                true
        );
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this); // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this); // Add this method.
    }

}
