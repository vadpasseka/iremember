package com.yemi.iremember.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.yemi.iremember.Constants;
import com.yemi.iremember.adapters.ContactAdapter;
import com.yemi.iremember.been.Contact;
import com.example.serhiy.calenarnot.R;
import com.yemi.iremember.dao.ContactDao;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
public class ContactPickerActivity extends Activity {


    private ContactAdapter dataAdapter;
    private HashMap<String, String> contacts = new HashMap<String, String>();

    @InjectView(R.id.tvToolbarAddRecipient)
    TextView title;

    @InjectView(R.id.inputSearch)
    EditText inputSearch;

    @InjectView(R.id.lvContacts)
    ListView lvContacts;

    List<Contact> contactList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_recipient_layout);
        ButterKnife.inject(this);
        Typeface regularTf = Typeface.createFromAsset(getAssets(), "MyriadProRegular.ttf");
        title.setTypeface(regularTf);
        inputSearch.setTypeface(regularTf);
        contacts = (HashMap<String, String>) getIntent().getSerializableExtra("contacts");
        displayListView();
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    @OnClick(R.id.btnCancel)
    public void cancelClick(){
        closeActivity();
    }

    @OnClick(R.id.back)
    public void backClick(){
        closeActivity();
    }

    @OnClick(R.id.btnSave)
    public void btnSaveClick(){
        finishPicking();
    }



    private void displayListView() {
        ContactDao dao = new ContactDao();
        contactList = dao.getContactList(getContentResolver());

        for (Map.Entry<String, String> entry : contacts.entrySet()) {
                for(Contact c : contactList){
                    if(entry.getKey().equals(c.getPhone())){
                        c.setChecked(true);
                    }
                }
        }

        dataAdapter = new ContactAdapter(this, R.layout.contact_view, contactList);
        lvContacts.setAdapter(dataAdapter);
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                dataAdapter.getFilter().filter(cs);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }
            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });

        lvContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CheckBox checkBox = (CheckBox)view.findViewById(R.id.chBoxIsChacked);
                contactList.get(i).setChecked(!checkBox.isChecked());
                checkBox.setChecked(!checkBox.isChecked());
            }
        });
    }

    private void closeActivity(){
        Intent intent = new Intent(getApplicationContext(), AddSmsActivity.class);
        intent.putExtra("contacts", contacts);
        setResult(Constants.CONTACT_PICKER_RESULT_CODE, intent);
        finish();
    }

    private void finishPicking(){
        Map<String, String> checked = dataAdapter.getChecked();
        Intent intent = new Intent(getApplicationContext(), AddSmsActivity.class);
        intent.putExtra("contacts", (HashMap<String, String>) checked);
        setResult(Constants.CONTACT_PICKER_RESULT_CODE, intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this); // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this); // Add this method.
    }

}
