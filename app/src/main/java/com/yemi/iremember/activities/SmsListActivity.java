package com.yemi.iremember.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.yemi.iremember.Constants;
import com.yemi.iremember.adapters.TaskAdapter;
import com.yemi.iremember.been.Task;
import com.example.serhiy.calenarnot.R;
import com.yemi.iremember.dao.TaskDao;
import com.yemi.iremember.preference.RememberPreferences;
import com.yemi.iremember.services.AlarmService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gemicle.core.beans.util.PopupUtil;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONObject;

import java.io.ObjectStreamException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import me.drakeet.materialdialog.MaterialDialog;

public class SmsListActivity extends Activity {

    private List<Task> tasks = new ArrayList<Task>();
    private TaskAdapter adapter;
    private BroadcastReceiver receiver;
    @InjectView(R.id.tvToolbarSmsList)
    TextView tvToolbarSmsList;
    @InjectView(R.id.addNewText)
    TextView addNewText;
    @InjectView(R.id.lvSms)
    ListView lvSms;

    private ObjectMapper mapper = new ObjectMapper();

    private RememberPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_list_layout);
        ButterKnife.inject(this);
        this.preferences = new RememberPreferences(this);
        Typeface regularTf = Typeface.createFromAsset(getAssets(), "MyriadProRegular.ttf");
        Typeface boldTf = Typeface.createFromAsset(getAssets(), "MyriadProBold.ttf");

        tvToolbarSmsList.setTypeface(regularTf);
        addNewText.setTypeface(regularTf);

        getTasks();
        createListView();
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        lvSms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Task task = tasks.get(i);
                if(task.getStatus().equals("sent")){
                    return;
                }
                Intent intent = new Intent(SmsListActivity.this, AddSmsActivity.class);
                try {
                    intent.putExtra("data", mapper.writeValueAsString(task));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                SmsListActivity.this.startActivity(intent);
            }
        });

        lvSms.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view,final int i, long l) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(SmsListActivity.this);
                dialog.setTitle(getResources().getString(R.string.title));
                dialog.setMessage(getResources().getString(R.string.message));
                dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TaskDao.init(getApplicationContext());
                        Task task = tasks.get(i);
                        TaskDao.deleteTaskById(task.getId());
                        AlarmService.removeIntent(getApplicationContext(), task.getIntentId());
                        getTasks();
                        refreshList(tasks);
                    }
                });
                dialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                return false;
            }
        });

        createListUpdater();

    }


    @OnClick(R.id.home)
    public void homeClick(){
        startActivity(new Intent(this, AdvertiseActivity.class));
    }

    @OnClick(value = {R.id.addNew, R.id.add})
    public void addNewClick(){
        close();
    }




    private void createListView(){
        adapter = new TaskAdapter(getApplicationContext(), R.id.checkbox, tasks);
        lvSms.setAdapter(adapter);
    }

    private void getTasks(){
        TaskDao.init(getApplicationContext());
        tasks = TaskDao.getTasks();
        Collections.sort(tasks, new Task());
    }

    private void close(){
        Intent i = new Intent(this, AddSmsActivity.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if(!preferences.isShowed()){
            PopupUtil.showDialog(this, R.string.popup_btn, R.string.popup_close, getResources().getString(R.string.popup_message), new PopupUtil.IDialogAction() {
                @Override
                public void accept(MaterialDialog mMaterialDialog) {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }

                @Override
                public void dismiss(MaterialDialog mMaterialDialog) {
                    mMaterialDialog.dismiss();
                }
            });
            preferences.setShowed(true);
        }
        else{
            startActivity(intent);
        }

    }

    public void refreshList(List<Task> tasks){
        adapter = (TaskAdapter)lvSms.getAdapter();
        if(adapter == null){
            return;
        }
        adapter.clear();
        int index = 0;
        for (Task task: tasks){
            adapter.insert(task, index ++);
        }
        adapter.notifyDataSetChanged();
    }

    private void createListUpdater() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getTasks();
                refreshList(tasks);
            }
        };
        IntentFilter intentFilter = new IntentFilter(Constants.INTENT_FILTER_ACTION);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this); // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this); // Add this method.
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
