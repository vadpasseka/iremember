package com.yemi.iremember.activities;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import com.example.serhiy.calenarnot.R;
import com.google.analytics.tracking.android.EasyTracker;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AdvertiseActivity extends Activity {

    @InjectView(R.id.webview)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertise);
        ButterKnife.inject(this);
        webView.clearCache(true);
        webView.loadUrl("http://yemi.info:8000/rock/index.html");
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this); // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this); // Add this method.
    }


}
