package com.yemi.iremember.services;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.yemi.iremember.been.Task;
import com.yemi.iremember.dao.TaskDao;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TaskService {


    public void displayDBTable(Context context){
        TaskDao.init(context);
        Cursor c = TaskDao.getCursor();
        if (c.moveToFirst()) {
            int id = c.getColumnIndex(TaskDao.TASK_ID);
            int taskId = c.getColumnIndex(TaskDao.TASK);
            int statusId = c.getColumnIndex(TaskDao.STATE);
            do {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    Task task = mapper.readValue(c.getString(taskId), Task.class);
                    String status = c.getString((statusId));
                    Log.i("task: ", status + "\t" + task.getDate() + "\t" + task.getMessage() + "\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } while (c.moveToNext());
        }
    }


}
