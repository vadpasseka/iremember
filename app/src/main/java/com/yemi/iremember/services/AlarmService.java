package com.yemi.iremember.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.yemi.iremember.Constants;
import com.yemi.iremember.Utils;
import com.yemi.iremember.receivers.AlarmManagerBroadcastReceiver;

public class AlarmService {

    public static void setAlarm(Context context, String date, String time,
                                Long taskId, int intentId){
        Long timestamp = Long.valueOf(Utils.getTimestamp(date, time));
        Intent intentAlarm = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intentAlarm.putExtra(Constants.SMS_ID, taskId);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, timestamp, PendingIntent.getBroadcast(context, intentId,
                intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));
    }

    public  static void removeIntent(Context context, int intentId){
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, intentId, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }




}
