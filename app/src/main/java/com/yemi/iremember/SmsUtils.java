package com.yemi.iremember;


import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.yemi.iremember.been.MessageStatus;
import com.yemi.iremember.dao.SmsDao;
import com.yemi.iremember.dao.TaskDao;
import com.yemi.iremember.receivers.SmsStatusBroadcastReceiver;
import com.yemi.iremember.services.AlarmService;

public class SmsUtils {

    public static void send(String message, String number, Context context, int taskId) {
        try{
            int intentId = taskId + 1;
            Intent intent = new Intent(context, SmsStatusBroadcastReceiver.class);
            intent.putExtra(Constants.SMS_ID, taskId);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, intentId,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            SmsManager sms = SmsManager.getDefault();

            sms.sendTextMessage(number, null, message, pendingIntent, null);
            Toast.makeText(context, message + "\n" + number, Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException e){
            Log.e("SMS_UTILS_ERROR", e.getMessage());
        }
    }

}

