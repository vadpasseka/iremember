package com.yemi.iremember.preference;

import android.content.Context;
import android.content.SharedPreferences;


public class RememberPreferences {

    private static final String KEY_REMEMBER = "remember";
    private static final String SHOWED = "showed";

    private SharedPreferences preferences;

    public RememberPreferences(Context context) {
        preferences = context
                .getSharedPreferences("iremember", Context.MODE_PRIVATE);
    }

    public void saveRemember(Boolean remember) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_REMEMBER, remember);
        editor.commit();
    }

    public boolean isRemember() {
        return preferences.getBoolean(KEY_REMEMBER, false);
    }

    public void setShowed(boolean showed){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SHOWED, showed);
        editor.commit();
    }

    public boolean isShowed(){
        return preferences.getBoolean(SHOWED, false);
    }


}