package com.yemi.iremember.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.yemi.iremember.been.Task;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

public class TaskDao extends SQLiteOpenHelper {
    static TaskDao instance = null;
    static SQLiteDatabase database = null;
	static final String DATABASE_NAME = "Notifier";
    static final  int DATABASE_VERSION = 15;
    public static final String TASK_TABLE = "tasks";
    public static final String TASK_ID = "_id";
	public static final String DATE = "date";
	public static final String STATE = "state";
	public static final String TASK = "task";

    public TaskDao(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TASK_TABLE + "("
                + TASK_ID + " integer primary key autoincrement, "
                + DATE +  " TEXT NOT NULL, "
                + TASK +  " TEXT NOT NULL, "
                + STATE + " TEXT NOT NULL "
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TASK_TABLE);
        onCreate(db);
    }

    public static void init(Context context){
        if(null == instance){
            instance = new TaskDao(context);
        }
    }

    public static SQLiteDatabase getDatabase(){
        if(null == database){
            database =  instance.getWritableDatabase();
        }
        return database;
    }

    public static void deactivate(){
        if(null != database && database.isOpen()){
            database.close();
        }
        database = null;
        instance = null;
    }

    public static long create(Task task){
        return getDatabase().insert(TASK_TABLE, null, convertTaskToMap(task));
    }

    public static long update(Task task, Integer id){
        return getDatabase().update(TASK_TABLE, convertTaskToMap(task), TASK_ID + "=" + id, null);
    }

    public static Cursor getCursor(){
        String[] columns = new String[]{
                TASK_ID,
                DATE,
                TASK,
                STATE
        };
        return  getDatabase().query(TASK_TABLE, columns, null, null, null, null, DATE);
    }

    public static List<Task> getTasks(){
        ObjectMapper mapper = new ObjectMapper();
        List<Task> tasks = new ArrayList<Task>();
        Cursor c = TaskDao.getCursor();
        if (c.moveToFirst()) {
            int id = c.getColumnIndex(TaskDao.TASK_ID);
            int taskId = c.getColumnIndex(TaskDao.TASK);
            int statusId = c.getColumnIndex(TaskDao.STATE);
            do {
                try {
                    Task task = mapper.readValue(c.getString(taskId), Task.class);
                    task.setId(c.getInt(id));
                    task.setStatus(c.getString(statusId));
                    tasks.add(task);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } while (c.moveToNext());
        }
        return tasks;
    }

    public static ContentValues convertTaskToMap(Task task){
        ObjectMapper mapper = new ObjectMapper();
        String t = null;
        try {
            t = mapper.writeValueAsString(task);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ContentValues cv = new ContentValues();
        cv.put(DATE, task.getTimestamp());
        cv.put(TASK, t);
        cv.put(STATE, task.getStatus());
        return cv;
    }

    public static void deleteTaskById(Integer id){
        Log.i("deleted task with id = ", String.valueOf(id));
        getDatabase().execSQL("delete from " + TASK_TABLE
                + " where " + TASK_ID + " = " + id);
    }

    public static void updateTaskById(Integer id, String status){
        getDatabase().execSQL("UPDATE " + TASK_TABLE
                + " SET "   + STATE     + " = " + "'" + status + "'"
                + " WHERE " + TASK_ID   + " = " + id);
    }


}
