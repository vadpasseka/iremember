package com.yemi.iremember.dao;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.yemi.iremember.been.Contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ContactDao  {

    public ArrayList<Contact> getContactList(ContentResolver cr){
        ArrayList<Contact> contactList = new ArrayList<>();
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection    = new String[] {
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor cursor = cr.query(uri, projection, null, null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME );
        int indexName = cursor.getColumnIndex(
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber = cursor.getColumnIndex(
                ContactsContract.CommonDataKinds.Phone.NUMBER);
        cursor.moveToFirst();
        do {
            contactList.add(new Contact(
                    cursor.getString(indexName),
                    cursor.getString(indexNumber),
                    cursor.getString(indexName).substring(0, 1), false));

        } while (cursor.moveToNext());
        cursor.close();
        return contactList;
    }

}
