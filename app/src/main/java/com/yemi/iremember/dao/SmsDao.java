package com.yemi.iremember.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;

public class SmsDao {

    public static void addMessageToSentFolder(String telNumber, String messageBody, ContentResolver contentResolver) {
        Uri SENT_MESSAGES_CONTENT_PROVIDER = Uri.parse("content://sms/sent");
        ContentValues sentSms = new ContentValues();
        sentSms.put("address", telNumber);
        sentSms.put("body", messageBody);
        contentResolver.insert(SENT_MESSAGES_CONTENT_PROVIDER, sentSms);
    }
}
