package com.yemi.iremember.listeners;


import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

public class ValidatorListener implements Validator.ValidationListener {

    private Context context;

    private ValidationCallback callback;

    public ValidatorListener(Context context, ValidationCallback callback){
        this.context = context;
        this.callback = callback;
    }

    @Override
    public void onValidationSucceeded() {
        callback.success();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            String message = error.getCollatedErrorMessage(context);
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            break;
        }
        callback.fail();
    }

}
