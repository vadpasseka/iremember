package com.yemi.iremember.listeners;


public interface ValidationCallback {

    void success();

    void fail();

}
