package com.yemi.iremember.adapters;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yemi.iremember.been.Contact;
import com.yemi.iremember.been.MessageStatus;
import com.yemi.iremember.been.Task;
import com.example.serhiy.calenarnot.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class TaskAdapter extends ArrayAdapter<Task>{

    private List<Task> objects;
    private Context context;
    private Filter filter;

    public TaskAdapter (Context context, int resourceId, List<Task> objects) {
        super(context, resourceId, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Task getItem(int position) {
        return objects.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.sms_item, null);
            holder = new ViewHolder();
            AssetManager assetManager = context.getResources().getAssets();
            final Typeface regularTf = Typeface.createFromAsset(assetManager, "MyriadProRegular.ttf");
            holder.ivProcess =  (ImageView) convertView.findViewById(R.id.ivProcess);
            holder.tvNames   =  (TextView) convertView.findViewById(R.id.tvNames);
            holder.tvSmsInfo =  (TextView) convertView.findViewById(R.id.tvSmsInfo);
            holder.tvDate    =  (TextView) convertView.findViewById(R.id.tvDate);
            holder.tvTime    =  (TextView) convertView.findViewById(R.id.tvTime);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Task task = objects.get(position);
        String contacts = task.getContacts().values().toString().replace("[", "").replace("]", "");
        holder.tvNames.setText(contacts);
        holder.tvSmsInfo.setText(task.getMessage());
        holder.tvDate.setText(task.getDate());
        holder.tvTime.setText(task.getTime());
        if(task.getStatus().equals(MessageStatus.SENT.getStatus())){
            holder.ivProcess.setImageResource(R.drawable.ic_action_check);
            holder.ivProcess.clearAnimation();
        }
        if(task.getStatus().equals(MessageStatus.FAIL.getStatus())){
            holder.ivProcess.setImageResource(R.drawable.ic_action_fail);
            holder.ivProcess.clearAnimation();
        }
        else if(task.getStatus().equals(MessageStatus.IN_PROGRESS.getStatus())){
            Animation a = new RotateAnimation(0.0f, 360.0f,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                    0.5f);
            a.setRepeatCount(-1);
            a.setDuration(1000);
            holder.ivProcess.startAnimation(a);
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView ivProcess;
        TextView tvNames;
        TextView tvSmsInfo;
        TextView tvDate;
        TextView tvTime;
    }


}
