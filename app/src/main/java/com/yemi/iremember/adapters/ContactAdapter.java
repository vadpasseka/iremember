package com.yemi.iremember.adapters;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yemi.iremember.been.Contact;
import com.example.serhiy.calenarnot.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import de.hdodenhof.circleimageview.CircleImageView;

public class ContactAdapter extends ArrayAdapter<Contact> {
    private List<Contact> objects;
    private Context context;
    private Filter filter;

    public ContactAdapter(Context context, int resourceId, List<Contact> objects) {
        super(context, resourceId, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Contact getItem(int position) {
        return objects.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.contact_view, null);
            holder = new ViewHolder();
            AssetManager assetManager = context.getResources().getAssets();
            final Typeface regularTf = Typeface.createFromAsset(assetManager, "MyriadProRegular.ttf");
            holder.firstLetter = (TextView) convertView.findViewById(R.id.firstLetterOfName);
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvNumber = (TextView) convertView.findViewById(R.id.tvNumber);
            holder.llChecked = (LinearLayout) convertView.findViewById(R.id.llChecked);
            holder.tvName.setTypeface(regularTf);
            holder.tvNumber.setTypeface(regularTf);
            holder.chBoxIsChacked = (CheckBox) convertView.findViewById(R.id.chBoxIsChacked);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Contact contact = objects.get(position);
        holder.firstLetter.setText(contact.getLatter());
        holder.tvNumber.setText(contact.getPhone());
        holder.tvName.setText(contact.getName());
        holder.chBoxIsChacked.setChecked(contact.getChecked());
        holder.chBoxIsChacked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox bx = (CheckBox) view;
                objects.get(position).setChecked(bx.isChecked());
            }
        });
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new AppFilter<>(objects);
        return filter;
    }

    private class AppFilter<T> extends Filter {

        private ArrayList<T> sourceObjects;

        public AppFilter(List<T> objects) {
            sourceObjects = new ArrayList<T>();
            synchronized (this) {
                sourceObjects.addAll(objects);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence chars) {
            String filterSeq = chars.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (filterSeq != null && filterSeq.length() > 0) {
                ArrayList<T> filter = new ArrayList<T>();

                for (T object : sourceObjects) {
                    if (object.toString().toLowerCase().contains(filterSeq))
                        filter.add(object);
                }
                result.count = filter.size();
                result.values = filter;
            } else {
                synchronized (this) {
                    result.values = sourceObjects;
                    result.count = sourceObjects.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            ArrayList<T> filtered = (ArrayList<T>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0, l = filtered.size(); i < l; i++)
                add((Contact) filtered.get(i));
            notifyDataSetInvalidated();
        }
    }

    private class ViewHolder {
        TextView firstLetter;
        TextView tvName;
        TextView tvNumber;
        LinearLayout llChecked;
        CheckBox chBoxIsChacked;
    }

    public Map<String, String> getChecked(){
        Map<String, String> checked = new HashMap<String, String>();
        for(Contact c : objects){
            if(c.getChecked()){
                checked.put(c.getPhone(), c.getName());
            }
        }
        return checked;
    }

}
